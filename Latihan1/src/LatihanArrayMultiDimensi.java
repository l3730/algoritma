/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lanis
 */
public class LatihanArrayMultiDimensi {
    public static void main(String[] args){
        String mahasiswa[][]={
            {
                "021210009", "Anselmus"
            },
            {
                "021210049", "Shelly"
            },
            {
                "021210067", "Epa"
            },
            {
                "021210065", "Alham"
            },
            {
                "021210039", "Riki"
            }
        };
        System.out.println("Nama "+ mahasiswa[2][1]+" " +"Berada pada baris 3, kolom 2");
        System.out.println(mahasiswa[3][0]+" " +"Berada pada baris 4, kolom 1");
         System.out.println(mahasiswa[4][1]+" " +"Berada pada baris 5, kolom 2");
    }
    
}

